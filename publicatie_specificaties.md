# Publicatie Specificaties

Voor de publicatie van de standaard specificaties gaan we uit van "best practices" van een de Open Web Standaarden waaronder:

- *ResPec* indien er formele standaarden moeten worden gedocumenteerd waaraan voldaan moet worden (conformance). Zie [Pub Tools > Md ResPec](https://gitlab.com/istddev/pub-tools/mdrespec) voor meer informatie en voorbeelden.

- *Widoco* voor de formele publicatie van Ontologie (PM voorbeelden)

- *API-interface* specificaties (n.t.b. welke en hoe)
