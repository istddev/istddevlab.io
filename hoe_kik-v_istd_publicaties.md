# Hoe zou een GitLab publicatie eruit kunnen zien?

```plantuml
@startmindmap
+ Vormgeving
++ Boekvorm
+++ Explainers
+++ Overig?
++ ResPec
+++ Proces standaarden
+++ Functionele standaarden
+++ Technische standaarden
++ Widoco
+++ Ontologie
++ Overige vormen?
@endmindmap
```
 

# Waaraan zou een GitLab publicatie aan moeten voldoen?

```plantuml
@startmindmap
+ Requirements
++ Afbakening rollen
+++ Analisten
+++ Reviewers
++ Versie strategie
+++ Wijzigingenbeheer vanuit modelleren?
+++ Preview opties
+++ Workflows onderhoud en publicatie
@endmindmap
```

# Aanpak opbouw publicatie

```plantuml
@startmindmap
+ Wat eerst?
++ Regels publiceren uitproberen
+++ Fabien - csv regels naar Markdown (proberen)
+++ Onno - ondersteunen
++ Schets maken van legacy iStandaarden
+++ Onno - maakt een demo
+++ Diagrammen
+++ Datatypes
+++ Codelijsten
++ Ontwerprichtlijnen document publiceren
+++ Onno - probeert dat (nu iets vergelijkbaars voor KIK-V Explainer)
@endmindmap
```
