# iStdDev.GitLab.Io

**Groepsite** voor verkenning publicatie standaarden.
  
Deze repository dient naast de inhoudelijke informatie ook als voorbeeld hoe een **groepsite** in **GitLab** kan worden opgezet. Via **GitLab Pages** is het namelijk mogelijk om voor elke groep een site te publiceren door:

- Onder de groep op basis van een **wildcart(*)** domein een **project** aan te maken. Zie verder [Groepsite project aanmaken](#Groepsiteprojectaanmaken)

- Een **Gitlab Pages publicatie** op basis van een **statisch raamwerk** te maken. Zie

## Groepsite project aanmaken

Om een groepsite aan te maken moet onder de groep een project worden aangemaakt die:

- start met de naam van de groep. In het voorbeeld van deze site dus **iStdDev**

- met daarachter het **wildcart(*)** domein. In het voorbeeld van deze site ***.Gitlab.Io**

Daarmee is de naam van dit voorbeeldproject voor de groepsite dus **iStdDev.GitLab.Io** geworden.

## Groepsite via GitLab Pages publiceren

Daarvoor is in dit voorbeeld gebruik gemaakt van het **statische VuePress-raamwerk**. Zie directorie **/docs** hoe de content en configuratie kan worden opgezet.
  
Het [GitLab CI script](.gitlab-ci.yml) zorgt ervoor dat bij elke wijzigiging (Commit naar Master) er een nieuwe **GitLab Pages** publicatie onder [https://istddev.gitlab.io/](https://istddev.gitlab.io/) wordt geplaatst.
  
NB: Dit script is nu zeer minimalistisch opgezet ter referentie voor deze verkenning.

