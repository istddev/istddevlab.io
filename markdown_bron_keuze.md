# Markdown als keuze voor bron-documenten

De eenvoudige **Markdown** notatie wordt in de meeste Open Source Ecosystemen reeds toegepast (en komt ook steeds vaker in vele commerciele toepassingen als opslag en/of export-formaat beschikbaar). Het is op dit moment het meest ondersteunde **content-formaat** voor raamwerken die uitgaan van **statische** web-publicaties. Een publicatie van een standaard is altijd een **statische** publicatie die na het einde van de levenscyclus wordt gerarchiveerd. Belangrijke voordeel voor uitwisselingsstandaarden zijn onder andere de uitgebreide ondersteuning die mogelijkehden biedt voor diagramweergaven en/of generatie via diverse Open Source Api's (PlantUML e.d.).

Zie eenvoudig sequence-digram in *PlantUML*.

```plantuml
Afnemer -> Aanbieder: SPARQL-request
Afnemer <- Aanbieder: SPARQL-response
```

Uiteindelijk is het van belang dat de **Markdown** als bron voor de **content** altijd in de huidige en toekomstige situatie kan worden hergebruikt. Gezien de brede ondersteuning en eenvoudige structuur lijkt dit een goede keuze in dat opzicht.
  
Wel moeten we nog een keuze maken welke **Flavour** (lees varianten tot uitbreiding) we gaan toepassen. De grote **GIT-repositories** zoals *GitHub* of het door het ZIN gebruikte *GitLab* een elk een eigen uitbreiding (Extension) op **Markdown**.
