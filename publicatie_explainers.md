# Publicatie Explainers

Een **Explainer** is min of meer informeel en overkoepelende uitleg over de (formeel) gespecificeerde standaard publicaties. Voor de publicatie van explianers gaan we uit van een zogenaamde Boek publicatie.
  
In de verkenning staat de informatie over de onderzochte publicatie-raamwerken en de voorlopige keuze die daarin is gemaakt. Zie [KIK-V Explainer voorbeeld](https://gitlab.com/istddev/kik-v/explainer).
