# Groepsite IstdDev Verkenning CI/ CD publicatie wijzen

## Over dit document

* [Leeswijzer](README.md)
* [Bijdragen](CONTRIBUTING.md)
* [Wijzigingen](CHANGELOG.md)

## Verkenningen

* [Sitestructuur](sitestructuur.md)
* [Best Practices](best_practices.md)
* [Markdown bron keuze](markdown_bron_keuze.md)
* [Publicatie expainers](publicatie_explainers.md)
* [Publicatie specificaties](publicatie_specificaties.md)

## Ontwerpen

* [Hoe publicatie vormgeven](hoe_kik-v_istd_publicaties.md)
