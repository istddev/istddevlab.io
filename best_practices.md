# Best Practices

De praktische uitgangspunten voor publicatie worden hieronder als (voorlopige) uitkomst van de verkenning samengevat:

- Er is een keuze gemaakt om het [Markdown-formaat](markdown_bron_keuze.md) als bron voor teksten en begeleidende schema's te gerbruiken .
  
- We gaan op momenteel uit van twee *soorten* publicaties:

  - [Explainers](publicatie_explainers.md) - die uitleg geven aan waar de standaard voor kan worden gebruikt en hoe de standaard kan worden toegepast.

  - [Specificaties](publicatie_specificaties.md) - de specificatie van de standaarden zelf zowel functioneel als technisch
