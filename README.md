# Leeswijzer

![Figuur 1: Logo](./logo.png)
  
Deze iStdDev-site is onderdeel voor een verkenning om te komen tot een op GitLab CI / CD gebasseeerde publicatie van digitale uitwisselingsstandaarden onder DIZRA (referentiearchitectuur van een duurzaam informatiestelsel in de zorg).

De volgende paragrafen bieden een ingang over de huidige inzichten zoals: de mogelijke structuur; en praktische uitgangspunten om publicaties tot stand te brengen.
  
Alles wat op deze site en onderliggende GitLab repositories staat is bedoeld als een voorbeeld ter inspiratie en evaluatie voor meerdere standaardiseringsinitiatieven.
  
**De inhoud is dus excpliciet niet bedoeld als documentatie maar als voorbeeld hoe e.a. er uit kan gaan zien in een publicatie**.

![Figuur 2: Site onder constructie](./assets/wip.svg)

Zet er wat bij
