# Sitestructuur

Deze [site](https://gitlab.com/istddev/istddev.gitlab.com) is een groepssite voor de [iStdDev](https://gitlab.com/istddev) groep met daarin de overall informatie over de verkenningen. Bedoeld als hoofdstructuur voor de vaste onderdelen bestaande uit de *subgroepen*:

* [Business](https://gitlab.com/istddev/business) - Verkenning DIZRA Ketenperspectief publicaties

* [Ecosysteem](https://istddev.gitlab.io/ecosysteem/explainer) - DIZRA Netwerk en Ecosysteemperspectief publicaties

* [KIK-V](https://gitlab.com/istddev/kik-v) - Verkenning KIK-V standaard publicaties

* [Pub Tools](https://gitlab.com/istddev/pub-tools) - Verkenning hergebruik Open Source publicatiecomponenten

Onder deze *subgroepen* staan meerdere verkenningen die nog lopen of reeds zijn afgerond.
